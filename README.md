This README will you explain how you can use the script for analysing the data.

It's important to get the dataset and name it "DataFinalProject.csv".
Make sure the script and the file named "DataFinalProject.csv" are in the same folder.
First you have to make the script executable.
You can do this by typing "chmod +x ScriptFinalProject.sh" in your terminal. Again it is important that you are in the folder where the document is saved.
Now you can run the script by typing "./ScriptFinalProject.sh".

If you have any questions about the program, you can send an email to the following adress: J.L.Bosman@student.rug.nl.