#!/bin/bash

echo 'MBO institution & Low income:'
cat DataFinalProject.csv | grep 'laag inkomen' | grep '2011' | cut -d';' -f6
echo 'University & Low income:'
cat DataFinalProject.csv | grep 'laag inkomen' | grep '2011' | cut -d';' -f7
echo 'MBO institution & High income:'
cat DataFinalProject.csv | grep 'hoog inkomen' | grep '2011' | cut -d';' -f6
echo 'University & High income:'
cat DataFinalProject.csv | grep 'hoog inkomen' | grep '2011' | cut -d';' -f7